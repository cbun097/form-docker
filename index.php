<!DOCTYPE html>
<html>
<head>
    <title>Inscription etudiant - programme tuto</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <h1 class="form-group col-md-10">Inscription etudiant</h1>
    <!--attache avec la page php a l'action-->
    <form name="studentForm" action="insert.php" method="POST">
        <div class="form-group col-md-10">
            <label>Numéro étudiant:</label>
            <input type="text" class="form-control" name="numetudiant" required>
        </div>
        <div class="form-row">
            <div class="form-group col-md-5">
                <label>Nom:</label>
                <input type="text" name="nom" class="form-control" required>
            </div>
            <div class="form-group col-md-5">
                <label>Prénom:</label>
                <input type="text" name="prenom" class="form-control" required>
            </div>
        </div>
        <div class="form-group col-md-10">
            <label>Adresse electronique:</label>
            <input type="text" name="email" class="form-control" required>
        </div>
        <div class="form-group col-md-10">
            <label>Programme:</label>
            <select name="programme" class="form-control" required>
                <option value="Technique informatique">Technique Informatique</option>
                <option value="Science humaine">Science Humaine</option>
                <option value="Science nature">Science naturel</option>
                <option value="science lettre et art">Science lettre et art</option>
            </select>
        </div>
        <div class="form-group col-md-10">
            <label>Cours:</label>
            <input type="text" name="cours" class="form-control" required> 
        </div>
        <div class="form-group col-md-6">
            <input type="submit" value="Envoyer" class="btn btn-default"> 
        </div>
    </form>
</body>
</html>
<!--Source:https://www.youtube.com/watch?v=qm4Eih_2p-M-->