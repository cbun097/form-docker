<?php
    //variables pour les elements de la page html
    $numEtudiant = $_POST['numetudiant'];
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $email = $_POST['email'];
    $programme = $_POST['programme'];
    $cours = $_POST['cours'];

    //faire la validation cote serveur
    if(!empty ($numEtudiant)||!empty($nom)||!empty($prenom) ||!empty($email)
    || !empty($programme) || !empty($cours))
    {
        //variables pour la connection
        $host="172.17.0.2";
        $dbUsername="root";
        $dbPassword="Password1!";
        $dbName ="tuto_programme";

        //create connection
        $conn = new mysqli($host, $dbUsername, $dbPassword, $dbName);
        if(mysqli_connect_error())
        {
            //erreur dans la connection
            die('Connect Error(' . mysqli_connect_error(). ')'. mysqli_connect_error());
        }
        else
        {
            //creer la table

            $SELECT = "SELECT NumEtudiant FROM Etudiants WHERE NumEtudiant = ? LIMIT 1";
            $INSERT = "INSERT INTO Etudiants (NumEtudiant,Nom, Prenom, Email, Programme, Cours) VALUES(?,?,?,?,?,?)";

            //prepare statement
            $stmt = $conn->prepare($SELECT);
            $stmt->bind_param("s", $numEtudiant);
            $stmt->execute();
            $stmt->bind_result($numEtudiant);
            $stmt->store_result();
            $rnum = $stmt->num_rows;

            if($rnum==0)
            {
                $stmt->close();

                $stmt = $conn->prepare($INSERT);
                $stmt->bind_param("ssssss", $numEtudiant,$nom, $prenom, $email, $programme, $cours);
                $stmt->execute();
                echo print "Votre enregistrement a été créée avec succès";
            }
            else
            {
                echo "Erreur! Vous ne pouvez pas vous inscrire 2 fois.";
            }
        }
        $stmt->close();
        $conn->close();
    }
    else
    {
        echo "Tous les champs sont obligatoires.";
        die();
    }
?>