# form-docker

Démonstration de Docker avec un formulaire en PHP et Mariadb

Demonstration of Docker with PHP form and Mariadb database

INSTALL DOCKER ON THE TERMINAL

sudo apt install docker.io
sudo systemctl start docker

PULL IMAGE

sudo docker pull mariadb
sudo docker pull php:7.2-apache

RUN CONTAINERS

sudo docker run -d -p 80:80 --name Apache -v $HOME/www:/var/www/html 
php:7.2-apache && sudo docker exec -ti Apache docker-php-ext-install
mysqli && sudo docker exec -ti Apache docker-php-ext-enable mysqli && 
sudo docker restart Apache

sudo docker run --name mariadb -e MYSQL_ROOT_PASSWORD=Password1! -d mariadb

GET MARIADB IP CONTAINER TO BE CONNECTED

sudo docker inspect mariadb | grep IPA

create the database call tuto_programme
create the table Etudiants with the same number of columns 

NOTE
To run the Apache container you can see multiples arguments. 
Mysqli needed to be added as an extension because of my code. 
Note the container for php is called Apache. 
You can change it after the --name but don't forget to change at the other
places as well. 